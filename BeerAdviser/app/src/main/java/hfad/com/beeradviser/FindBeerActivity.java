package hfad.com.beeradviser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class FindBeerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_beer);
    }

    public void onClickFindBeer(View view) {
        TextView brands = (TextView) findViewById(R.id.brands);
        brands.setText("");
        Spinner color = (Spinner) findViewById(R.id.spinner);
        String beerType =String.valueOf(color.getSelectedItem());
        BeerExpert beerExpert = new BeerExpert();
        List<String> beers = beerExpert.getBrands(beerType);
        for (String beer:beers){
            brands.append(beer + "\n");
        };
    }
}
