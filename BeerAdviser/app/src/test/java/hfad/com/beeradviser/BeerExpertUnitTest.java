package hfad.com.beeradviser;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BeerExpertUnitTest {
    BeerExpert beerExpert = new BeerExpert();
    @Test
    public void getBrandsAmber(){
      List<String> expectedColors = new ArrayList<>();
      expectedColors.add("Jack Amber");
      expectedColors.add("Red Moose");
      List<String> colors = beerExpert.getBrands("amber");
      assertTrue("As cores devem ser iguais", expectedColors.equals(colors));
   }
   @Test
    public void getBrandsNotAmber(){
        List<String> expectedColors = new ArrayList<>();
        expectedColors.add("Jail Pale Ale");
        expectedColors.add("Gout Stout");
        List<String> colors = beerExpert.getBrands("pale");
        assertTrue(expectedColors.equals(colors));
    }

}
