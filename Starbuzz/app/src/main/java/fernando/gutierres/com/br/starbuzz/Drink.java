package fernando.gutierres.com.br.starbuzz;

public class Drink {

    private String name;
    private String description;
    private int imageResourceId;

    public static final Drink[] drinks = {
            new Drink("Late", "Ac ouple of espresso hots with steamed milk",R.drawable.latte),
            new Drink("Capuccino", "Espresso, hot milk, and a steamed milk form",R.drawable.capuccino),
            new Drink("Filter", "Highest quality beans roasted and brewd fresh",R.drawable.filter)
    };

    private Drink(String name, String description, int imageResourceId){
        this.name = name;
        this.description = description;
        this.imageResourceId =imageResourceId;
    }

    public String getDescription(){
        return description;
    }

    public String getName(){
        return name;
    }

    public int getImageResourceId(){
        return imageResourceId;
    }

    public String toString(){
        return this.name;
    }
}
