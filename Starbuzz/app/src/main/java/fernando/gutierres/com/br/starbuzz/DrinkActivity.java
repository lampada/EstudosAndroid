package fernando.gutierres.com.br.starbuzz;

import android.media.Image;
import android.os.Bundle;
import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DrinkActivity extends Activity {

    public static final String EXTRA_DRINKNO = "drinkNo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);
        int drinkNo = (Integer) getIntent().getExtras().get(EXTRA_DRINKNO);

        Drink drink = Drink.drinks[drinkNo];

        ImageView imageView = (ImageView)findViewById(R.id.photo);
        imageView.setImageResource(drink.getImageResourceId());
        imageView.setContentDescription(drink.getName());

        TextView drinkName = (TextView)findViewById(R.id.name);
        drinkName.setText(drink.getName());

        TextView description =(TextView) findViewById(R.id.description);
        description.setText(drink.getDescription());
    }

}
